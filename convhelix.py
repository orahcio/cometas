import numpy as np
from scipy.signal import fftconvolve
import matplotlib.pyplot as plt
import timeit
import pandas as pd

from photutils import CircularAperture, aperture_photometry


def I(x,y):
    '''
    Função da coma
    '''

    r = np.sqrt(x*x+y*y)
    return k2/(1+np.power(r,m))

def moffat(x,y):
    '''
    PSF Moffat
    '''
    r = np.sqrt(x*x+y*y)/theta
    r *= r
    return I0/(1+np.power(r,beta))


def zero_pad_1D_convolve(X, Y):
    '''
    Esta função transforma as matrizes X e Y, método de hélice:
    1 - Zero padding para ficarem quadradas e com mesmo número de linhas e colunas;
    2 - Faz um vetor 1D a partir da matriz, truncado no última elemento da matriz, percorrendo linhas
        antes de colunas;
    3 - Efetua a convolução em 1D;
    4 - Transforma o resultado novamente em uma matriz
    '''

    M, N = X.shape
    K, L = Y.shape

    Xlen = (N - 1) * (M + K - 1) + M
    Ylen = (L - 1) * (M + K - 1) + K

    # 1 - Zero padding
    X_pad = np.pad(X, ((0,K-1),(0,L-1)), 'constant')
    Y_pad = np.pad(Y, ((0,M-1),(0,N-1)), 'constant')

    # 2
    padlen = (M+K-1)*(N+L-1)
    # 2 Reshape and truncate
    Xre = X_pad.reshape(padlen, order='F')[:Xlen]
    Yre = Y_pad.reshape(padlen, order='F')[:Ylen]

    # 3 - Convolve
    #Z = np.convolve(Xre,Yre)
    Z = fftconvolve(Xre, Yre) # Muito mais rápido via fft

    return Z.reshape(X_pad.shape, order='F')


def fluxos(im, p):
    '''
    Contabiliza os fluxos de uma imagem a partir dos valores de rai em p
    '''


    i, j = im.shape
    i = int(i/2)
    j = int(j/2)

    apertures = [CircularAperture((i,j), r) for r in p]
    phot = aperture_photometry(im, apertures)

    return phot.to_pandas().iloc[0,3:].values


if __name__ == "__main__":

    # Parâmetros do modelo
    m = 1.5
    beta = 4.7
    theta = 3.
    I0 = 1.
    k2 = 1.

    arcsecpixel = 0.71
    total = 201
    a = arcsecpixel*total/2
    nk = total # PSF com mesma resolução que a coma

    x = np.linspace(-a,a, total)
    xk = np.linspace(-a,a,total)
    y = np.linspace(-a,a, total)
    yk = np.linspace(-a,a,total)
    xx, yy = np.meshgrid(x, y, sparse=True)
    im = I(xx,yy)
    xx, yy = np.meshgrid(xk, yk, sparse=True)
    PSF = moffat(xx,yy)

    start = timeit.default_timer()
    Z = zero_pad_1D_convolve(im,PSF)
    elapsed = timeit.default_timer() - start

    print('t: ', elapsed)

    # valores de raio para medir o fluxo
    p = np.array([7.1, 14.1, 21.2, 28.3, 35.4, 42.4])
    fluxos = fluxos(Z, p/arcsecpixel)

    with pd.ExcelWriter('fluxos.xlsx', mode='a') as writer:
        table = pd.DataFrame({'p': p, 'fluxos': fluxos})
        table.to_excel(writer, index=False, sheet_name='m='+str(m))

    # plt.imshow(im, cmap='gray')
    # plt.show()

    # plt.imshow(PSF, cmap='gray')
    # plt.show()

    # plt.imshow(Z, cmap='gray')
    # plt.show()