# Atenção crie uma senha de app no google pra poder usar o acesso so email

from urllib import request
from datetime import datetime
import os
import re
import sqlite3
from sqlite3 import Error
from astropy.time import Time
import numpy as np
import pandas as pd
import smtplib
from email.message import EmailMessage
import imaplib
from email import message_from_bytes
from DES import DES


def create_conn():

    conn = None

    try:
        conn = sqlite3.connect('fluxos.db')
    except Error as e:
        print(e)
    
    return conn


def create_datatable():
    '''
    Linha que obtem na primeira referência
    OBJECT        DATE       TIME        +/-    +/-    +/-    +/-    +/-    +/-     N  FWHM  CAT
    ------------  ---------- --------  -----  -----  -----  -----  -----  -----  ----  ----  ---
    C/2006 S3     25/05/2011 01:22:45  15.77  14.83  14.37  13.95  13.65  13.41   5.5  17.6  C23
    C/2006 S3     25/05/2011 01:22:45*  0.00   0.00   0.00   0.00   0.00   0.00     1   3.1  CMC

    Linha obtida com o horizons
    r,       rdot,            delta,     deldot,    S-T-O,
    '''

    try:
        create_table = '''CREATE TABLE IF NOT EXISTS fluxos ( 
                            id integer PRIMARY KEY,
                            object text,
                            datetime text,
                            F10x10 real,
                            eF10x10 real,
                            F20x20 real,
                            eF20x20 real,
                            F30x30 real,
                            eF30x30 real,
                            F40x40 real,
                            eF40x40 real,
                            F50x50 real,
                            eF50x50 real,
                            F60x60 real,
                            eF60x60 real,
                            snr real,
                            n integer,
                            sb real,
                            fwhm real,
                            cod text,
                            cat text,
                            r real,
                            rdot real,
                            delta real,
                            deltadot real,
                            sto real
                        );'''

        with create_conn() as conn:
            c = conn.cursor()
            c.execute(create_table)
            conn.commit()
            c.close()
    
    except Error as e:
        print(e)


def convfloat(x):
        try:
            return float(x)
        except ValueError:
            return None


def convint(x):
        try:
            return int(x)
        except ValueError:
            return None


def create_fluxos0(conn, dados):
    """
    Adicionar na tabela de fluxox os primeiros dados colhidos na Astronomie.be
    Primeira etapa
    """
    sql = ''' INSERT INTO fluxos(object,datetime,F10x10,F20x20,F30x30,F40x40,F50x50,F60x60,snr,sb,cod)
              VALUES(?,?,?,?,?,?,?,?,?,?,?) '''

    cur = conn.cursor()
    cur.executemany(sql, dados)
    conn.commit()

    return cur.rowcount


def update_fluxos1(conn, lista_dados):
    '''
    Adicionar valores em cada coluna identificando o id
    '''

    sql = '''UPDATE fluxos
             SET eF10x10 = ?,
                 eF20x20 = ?,
                 eF30x30 = ?,
                 eF40x40 = ?,
                 eF50x50 = ?,
                 eF60x60 = ?,
                 n = ?,
                 fwhm = ?,
                 cat = ?
             WHERE object = ? AND datetime = ?;'''
    
    cur = conn.cursor()
    cur.executemany(sql,lista_dados)
    conn.commit()

    return cur.rowcount


def update_fluxos3(email):
    '''
    Extrair dados dos emails a partir da string fornecida
    '''

    # Padrão pra coletar o nome do objeto
    patt = re.compile("\nTarget body name: (.*)\{.*\n")
    objname = patt.findall(email)[0].strip().split('/')[0]
    print(objname)

    # Padrão para coletar tabela
    patt = re.compile('\n\s(\d{4}-\w{3}-\d\d\s\d\d:\d\d:\d\d\.\d{3}),\s,\s,(.*),(.*),(.*),(.*),(.*),')
    lista_dados = []
    meses = {'Jan': 1, 'Feb': 2, 'Mar': 3, 'Apr': 4, 'May': 5,
            'Jun': 6, 'Jul': 7, 'Aug': 8, 'Sep': 9, 'Oct': 10,
            'Nov': 11, 'Dec': 12} # para traduzir os meses nas tabelas do email

    for date, r, rdot, delta, deltadot, sto in patt.findall(email): 
        # print(date, r, rdot, delta, deltadot, sto)
        y, mo, d, h, m, s, mic = re.split(':| |-|\.',date)
        dt = datetime(int(y),int(meses[mo]),int(d),int(h),int(m),int(s))
        lista_dados.append((r,rdot,delta,deltadot,sto,dt))

    sql = """UPDATE fluxos
             SET r = ?,
                 rdot = ?,
                 delta = ?,
                 deltadot = ?,
                 sto = ?
             WHERE datetime = ?;"""
    
    with create_conn() as conn:
        cur = conn.cursor()
        cur.executemany(sql,lista_dados)
        conn.commit()
        
        return cur.rowcount


def fase0(htmlstr):
    '''
    Fase 0: Padrão para fluxos de vários boxes
    '''

    patt = re.compile("\n(.{14})(\d\d\/\d\d\/\d{4} \d\d:\d\d:\d\d)\s(.{6})\s(.{6})\s(.{6})\s(.{6})\s(.{6})\s(.{6})\s(.{5})\s(.{5})\s(.{4})\n")
    lista_dados = patt.findall(htmlstr)
    # print(len(lista_dados), len(htmlstr))
    linhas = []

    for o,dt,v1,v2,v3,v4,v5,v6,v7,v8,v9 in lista_dados:
        d, mo, y, h, m, s = re.split(':| |\/',dt)
        dt = datetime(int(y),int(mo),int(d),int(h),int(m),int(s))
        o = o.strip().upper()
        v1 = convfloat(v1.strip())
        v2 = convfloat(v2.strip())
        v3 = convfloat(v3.strip())
        v4 = convfloat(v4.strip())
        v5 = convfloat(v5.strip())
        v6 = convfloat(v6.strip())
        v7 = convfloat(v7.strip())
        v8 = convfloat(v8.strip())
        v9 = v9.strip()
        # print(o, dt,v1,v2,v3,v4,v5,v6,v7,v8,v9)
        linhas.append([o,dt,v1,v2,v3,v4,v5,v6,v7,v8,v9])

    with create_conn() as conn:
        num = create_fluxos0(conn,linhas)
        # print(num)

    return num


def fase1(htmlstr):
    '''
    Fase 1: Padrão para os erros das grandezas acima
    '''

    patt = re.compile("(.{14})(\d\d\/\d\d\/\d{4} \d\d:\d\d:\d\d)\s{3}(.{4})\s(.{6})\s(.{6})\s(.{6})\s(.{6})\s(.{6})\s(.{5})\s(.{5})\s(.{4})\n")
    lista_dados = patt.findall(htmlstr)
    print(len(lista_dados))
    ldata = []
    for o,dt,v1,v2,v3,v4,v5,v6,v7,v8,v9 in lista_dados:
        d, mo, y, h, m, s = re.split(':| |\/',dt)
        dt = datetime(int(y),int(mo),int(d),int(h),int(m),int(s))
        o = o.strip()
        v1 = convfloat(v1.strip())
        v2 = convfloat(v2.strip())
        v3 = convfloat(v3.strip())
        v4 = convfloat(v4.strip())
        v5 = convfloat(v5.strip())
        v6 = convfloat(v6.strip())
        v7 = convint(v7.strip())
        v8 = convfloat(v8.strip())
        v9 = v9.strip()
        # print(o, dt,v1,v2,v3,v4,v5,v6,v7,v8,v9,nid)
        ldata.append((v1,v2,v3,v4,v5,v6,v7,v8,v9,o,dt))
    
    with create_conn() as conn:
        num = update_fluxos1(conn, ldata)

    return num


def fase2(objname):
    '''
    Fase 2: Requisição, lê as datas da tabela atual e envia email
            ao horizons para requisitar dados
    '''
    # Obtendo lista de tempos do objeto
    with create_conn() as conn:
        sql = '''SELECT datetime FROM fluxos WHERE object = '%s';''' % (objname)
        tabela = pd.read_sql_query(sql, conn)['datetime'].values.astype(np.datetime64)

    tlist = "'\n'".join([str(i.jd) for i in Time(tabela)])
    message = """\
!$$SOF
EMAIL_ADDR = \'%s\'
COMMAND = \'%s;\'
OBJ_DATA = \'%s\'
MAKE_EPHEM = \'%s\'
TABLE_TYPE = \'%s\'
CENTER = \'%s\'
REF_PLANE = \'%s\'
COORD_TYPE = \'%s\'
TLIST = \'%s\'
QUANTITIES = \'%s\'
CSV_FORMAT = \'%s\'
!$$EOF""" % ('orahcio@gmail.com', DES[objname], 'NO', 'YES', 'OBS', 'Geocentric',
            'ECLIPTIC', 'GEODETIC', tlist, '19,20,24', 'YES')

    # Compondo email
    msg = EmailMessage()
    msg['Subject'] = 'JOB'
    msg['From'] = ['orahcio@gmail.com']
    msg['To'] =  ['horizons@ssd.jpl.nasa.gov'] #', '.join(['horizons@ssd.jpl.nasa.gov', 'orahcio@gmail.com'])
    msg.set_content(message)

    # Iniciando servidor e enviando a requisição
    with smtplib.SMTP('smtp.gmail.com', 587) as server:
        server.starttls()
        server.login("orahcio@gmail.com", "")
        aux = server.send_message(msg)

    # print(msg)
    print(aux)

    return tabela.shape[0]


def fase3():
    """
    Ler e-mail e identificar quais corpos foram incluídos requisitados nesses emails
    """

    with imaplib.IMAP4_SSL('imap.gmail.com') as mail:
        mail.login('orahcio@gmail.com', '')
        mail.select("horizons") # connect to inbox.

        typ, mails_id = mail.uid('search', None, '(HEADER From "horizons@ssd.jpl.nasa.gov")')
        list_ids = mails_id[0].split()

        for ids in list_ids:
            result, data = mail.uid('fetch', ids, '(RFC822)')
            raw_email = data[0][1]

            corpo = message_from_bytes(raw_email)
            update_fluxos3(corpo.get_payload())

# 4P/Faye                         {source: JPL#K144
# 17P/Holmes                      {source: JPL#K212
def main():
    
    # os.system('rm fluxos.db')
    # create_datatable()

    # cometas = []
    # with open('cometas_links2.txt') as f:
        # for l in f:
            # cometas.append(l.strip())
    # cometas = cometas[:1]
    # Identificando o cometa
    # for url in cometas:
    #     print(url)
    #     page = re.sub('\*',' ', request.urlopen(url).read().decode("utf-8"))
    #     patname = re.compile("\n(.{14})\d\d\/")
    #     name = re.sub('\/','_',patname.findall(page)[0]).strip()
    #     print(name)

    #     num = fase0(page)
    #     print(num)

    #     num = fase1(page)
    #     print(num)

    #     if not name.upper() in DES:
    #         DES[name] = name

    # print(DES)

    # for i in DES:
    #     num = fase2(i)
    #     print(num)
    
    fase3()


if __name__ == '__main__':
    main()



# page = """\
# COD C23
# CATALOG: USNO A2.0 / CMC-14 - BAND: R   

#                                    10x10  20x20  30x30  40x40  50x50  60x60   SNR   SB   COD
# OBJECT        DATE       TIME        +/-    +/-    +/-    +/-    +/-    +/-     N  FWHM  CAT
# ------------  ---------- --------  -----  -----  -----  -----  -----  -----  ----  ----  ---
# 2P            12/08/2013 01:44:41  18.38  17.77  17.41  17.06  16.71  16.41   3.5  18.8  C23
# 2P            12/08/2013 01:44:41*  0.20   0.28   0.38   0.45   0.45   0.48     7   4.9  CMC

# 2P            16/08/2013 01:10:59  18.36  17.57  16.94  16.65  16.37  16.18   2.7  18.6  C23
# 2P            16/08/2013 01:10:59*  0.40   1.01   0.73   0.68   0.64   0.57     6   4.1  CMC

# 2P            29/08/2013 23:26:08  17.84  17.24  17.22                        3.5  17.4  C23
# 2P            29/08/2013 23:26:08*  0.38   0.27   0.13                          5   4.9  CMC
# """